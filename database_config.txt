Execute below command on db container to grant root previliges. 

mysql -u root -psupp0rt -e "ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY 'supp0rt';"

Or directly fire below command from docker server:- 

docker exec db mysql -u root -psupp0rt -e "ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY 'supp0rt';"

Now go to webserver & fire below command to check db connection. 

[root@1a1d24c517ff /]# mysql -h db -P 3306 -u root -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 14
Server version: 8.0.19 MySQL Community Server - GPL

Copyright (c) 2000, 2019, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| edureka            |
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
5 rows in set (0.00 sec)

mysql>


